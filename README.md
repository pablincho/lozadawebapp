# Requirements for run and develop Lozada WebApp

## Install requirement
Pipenv
```bash
pip install pipenv
```
Postgres SQL (Linux)
```bash
apt-get install postgresql postgresql-contrib

update-rc.d postgresql enable

service postgresql start
```

Postgres SQL (Mac)
```bash
brew install postgresql
brew services start postgresql
```
Other Dependencies
```bash
apt install python-psycopg2
apt install libpq-dev
```

## Create db on postgres
Access to postgres to verify, then exit with \q
```bash
sudo -u postgres psql
```
Create db
```bash
sudo -u postgres createdb leiadb
```
Export Enviornment Variable
```bash
export DATABASE_URL=postgresql:///leiadb
```

# Run Flask
## Activate enviroment ???
Upgrade DB
```bash
flask db upgrade
```
Insert Roles
```bash
flask users insert-roles
```
Create Admin user. Put password
```bash
flask users insert-admin-user 
```



#Add table to DB
Add model into models.py. Then run:
```bash
flask db migrate -m "Comment"
flask db upgrade
```
Check inside migrations->versions a new file named as the comment

To downgrade a version
```bash
flask db downgrade
```
#Deploy to Heroku
Commit and push all changes to repo then:
```bash
git push heroku master
```

To run a command in heroku:
```bash
heroku run flask db upgrade
```

import os

# Retrieve DATABASE_URL and adjust the prefix if necessary
uri = os.getenv("DATABASE_URL")  # Get the DATABASE_URL environment variable
if uri and uri.startswith("postgres://"):
    uri = uri.replace("postgres://", "postgresql://", 1)  # Adjust for SQLAlchemy

SQLALCHEMY_DATABASE_URI = uri

class Config(object):
    DEBUG = os.environ.get("DEBUG") or True
    SECRET_KEY = os.environ.get("SECRET_KEY") or "hard_to_guess_string"
    SQLALCHEMY_DATABASE_URI = SQLALCHEMY_DATABASE_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False

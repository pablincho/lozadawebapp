"""Excel contents table

Revision ID: 325a74cf59e5
Revises: 88fc2c3f04bf
Create Date: 2020-02-07 10:39:14.840843

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '325a74cf59e5'
down_revision = '88fc2c3f04bf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('excel_contents',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('dni', sa.Text(), nullable=True),
    sa.Column('codigo', sa.Text(), nullable=True),
    sa.Column('cliente', sa.Text(), nullable=True),
    sa.Column('ultimo_pago', sa.Text(), nullable=True),
    sa.Column('pagado_total', sa.Text(), nullable=True),
    sa.Column('ultima_gestion', sa.Text(), nullable=True),
    sa.Column('compromiso', sa.Text(), nullable=True),
    sa.Column('filtro_sugerido', sa.Text(), nullable=True),
    sa.Column('prioridad', sa.Text(), nullable=True),
    sa.Column('forma_de_pago', sa.Text(), nullable=True),
    sa.Column('vencimiento_acordado', sa.Text(), nullable=True),
    sa.Column('cuota_no_pagada', sa.Text(), nullable=True),
    sa.Column('estado_del_pp', sa.Text(), nullable=True),
    sa.Column('cuotas_pagadas_pp_activo', sa.Text(), nullable=True),
    sa.Column('saldo_cuota_aun_no_pago', sa.Text(), nullable=True),
    sa.Column('cuotas_orig_pp_activo', sa.Text(), nullable=True),
    sa.Column('cuota_original', sa.Text(), nullable=True),
    sa.Column('a_120_dias', sa.Text(), nullable=True),
    sa.Column('saldo_plan', sa.Text(), nullable=True),
    sa.Column('quita_max', sa.Text(), nullable=True),
    sa.Column('meta', sa.Text(), nullable=True),
    sa.Column('filtro', sa.Text(), nullable=True),
    sa.Column('lab', sa.Text(), nullable=True),
    sa.Column('stn', sa.Text(), nullable=True),
    sa.Column('asignacion', sa.Text(), nullable=True),
    sa.Column('en_el_est', sa.Text(), nullable=True),
    sa.Column('padron', sa.Text(), nullable=True),
    sa.Column('gestion', sa.Text(), nullable=True),
    sa.Column('observaciones', sa.Text(), nullable=True),
    sa.Column('ult_gest', sa.Text(), nullable=True),
    sa.Column('g', sa.Text(), nullable=True),
    sa.Column('ult_llam', sa.Text(), nullable=True),
    sa.Column('localizado', sa.Text(), nullable=True),
    sa.Column('importante', sa.Text(), nullable=True),
    sa.Column('teleprom', sa.Text(), nullable=True),
    sa.Column('tel_fliar', sa.Text(), nullable=True),
    sa.Column('tel_laboral', sa.Text(), nullable=True),
    sa.Column('dir_1', sa.Text(), nullable=True),
    sa.Column('operador', sa.Text(), nullable=True),
    sa.Column('email', sa.Text(), nullable=True),
    sa.Column('email_laboral', sa.Text(), nullable=True),
    sa.Column('hoy', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('excel_contents')
    # ### end Alembic commands ###

"""Users conditions table

Revision ID: 32ab2ab06916
Revises: 6cf9c728eee1
Create Date: 2020-01-06 11:27:07.277216

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '32ab2ab06916'
down_revision = '6cf9c728eee1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('conditions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=100), nullable=True),
    sa.Column('assigned_time', sa.Integer(), nullable=True),
    sa.Column('total_debt', sa.Integer(), nullable=True),
    sa.Column('interests_discount', sa.Integer(), nullable=True),
    sa.Column('capital_discount', sa.Integer(), nullable=True),
    sa.Column('installments_number', sa.Integer(), nullable=True),
    sa.Column('expiration_date', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('code')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('conditions')
    # ### end Alembic commands ###

"""Drop columns from User

Revision ID: 695428c69b74
Revises: 7abbd57f74be
Create Date: 2024-11-10 12:37:49.581311

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '695428c69b74'
down_revision = '7abbd57f74be'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_column('server_username')
        batch_op.drop_column('op_name')
        batch_op.drop_column('got_refinancing')
        batch_op.drop_column('op_phone')
        batch_op.drop_column('assignment_date')
        batch_op.drop_column('pp_status')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('pp_status', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
        batch_op.add_column(sa.Column('assignment_date', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
        batch_op.add_column(sa.Column('op_phone', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
        batch_op.add_column(sa.Column('got_refinancing', sa.BOOLEAN(), autoincrement=False, nullable=True))
        batch_op.add_column(sa.Column('op_name', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
        batch_op.add_column(sa.Column('server_username', sa.VARCHAR(length=100), autoincrement=False, nullable=True))

    # ### end Alembic commands ###

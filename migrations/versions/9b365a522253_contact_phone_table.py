"""Contact phone table

Revision ID: 9b365a522253
Revises: 3e1034498dbf
Create Date: 2019-11-27 20:20:36.727700

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9b365a522253'
down_revision = '3e1034498dbf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('contact_phone',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('phone', sa.String(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('contact_phone')
    # ### end Alembic commands ###

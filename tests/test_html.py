from requests_html import HTML
import urllib
import datetime


def test_basic_data_part_1():
    page = urllib.request.urlopen("file:///home/pablo/lozadawebapp/tests/html_test_files/datos_basicos_parte_1.html") \
        .read()
    r = HTML(html=page)
    assert r.search("<script>selCliente('{}');</script>")[0] == '3363979'


def test_basic_data_part_2():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/datos_basicos_parte_2.html").read()
    r = HTML(html=page)

    assert r.find('#Apellido_contacto', first=True).text == 'FOCES'
    assert r.find('#Nombre_contacto', first=True).text == 'FRANCISCO JAVIER'
    assert r.find('.destacado')[1].text == 'DU \xa025469313'
    assert r.search("'NAR ','{}   ',")[0] == "DU00025469313"
    assert r.search("   ','{}');\">Naranja")[0] == '20160210'
    tabla_sucursal = r.find('.tabla')[0]
    assert tabla_sucursal.find('td')[9].text == 'SAN FRANCISCO'


def test_basic_data_part_3():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/datos_basicos_parte_3.html").read()
    r = HTML(html=page)
    try:
        refinancing = r.search('Refinanciado: <B> * {} *')[0]
    except TypeError:
        refinancing = None
    assert refinancing is None
    risk = r.find('#Riesgo_contacto', first=True)
    assert risk.text == 'NAR-ALTO'


def test_payment_data():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/datos_de_pagos.html").read()
    r = HTML(html=page)
    total_debt = \
        r.search("Total Deuda\n</td>\n<td class=\"Dato\">\n<font class='destacado'><b>{}</b></font>")[0]
    payments_data_search = r.search_all("'ProductoPagoDetalle.asp','{}');\">")

    payment_date = []
    for data in payments_data_search:
        payment_date.append(data.fixed[0])

    payments_table = r.find("#tblPagos")[0]

    table_list = []

    table_body = payments_table.find("tr")
    for index, table_row in enumerate(table_body):
        element_list = []
        if index == 1:
            table_th = table_row.find('th')
            for element in table_th:
                element_list.append(element.text)

        elif index > 1:
            table_td = table_row.find('td')
            for element in table_td:
                element_list.append(element.text)

        table_list.append(element_list)
    assert total_debt == '650,91'
    assert table_list == [[], ['Fecha', 'Fecha Procesamiento', 'Total', 'Capital', 'Intereses', 'Gastos', 'Honorarios',
                               'Estado', 'Fecha Arqueo', 'Nro. Recibo', 'Ingresado por'], ['27/06/2017', '27/06/2017',
                                                                                           '5000', '2011.9', '2280.82',
                                                                                           '0', '707.28', 'Vigente',
                                                                                           '27/06/2017', '5665',
                                                                                           'Estudio Abog.'],
                          ['07/07/2017', '08/07/2017', '1151.49', '458.2', '579.86', '0',
                           '113.43', 'Vigente', '08/07/2017', '5721', 'Estudio Abog.'], ['08/08/2017', '09/08/2017',
                                                                                         '1151.49', '458.2', '579.86',
                                                                                         '0', '113.43', 'Vigente',
                                                                                         '09/08/2017', '5857',
                                                                                         'Estudio Abog.'],
                          ['11/09/2017', '12/09/2017', '1151.49', '458.2', '579.86', '0',
                           '113.43', 'Vigente', '12/09/2017', '6055', 'Estudio Abog.'], ['11/10/2017',
                                                                                         '12/10/2017', '1151.49',
                                                                                         '457.94', '580.12', '0',
                                                                                         '113.43', 'Vigente',
                                                                                         '12/10/2017',
                                                                                         '6213', 'Estudio Abog.'],
                          ['18/10/2017', '18/10/2017', '0.65', '0.26', '0.33', '0',
                           '0.06', 'Vigente', '18/10/2017', '6247', 'Estudio Abog.'], ['07/11/2017', '08/11/2017',
                                                                                       '1151.49', '458.2', '579.86',
                                                                                       '0', '113.43', 'Vigente',
                                                                                       '08/11/2017', '6344',
                                                                                       'Estudio Abog.'],
                          ['11/12/2017', '12/12/2017', '1151.49', '458.2', '579.86', '0',
                           '113.43', 'Vigente', '12/12/2017', '6501', 'Estudio Abog.'], ['15/01/2018', '16/01/2018',
                                                                                         '1151.49', '456.91', '581.18',
                                                                                         '0', '113.4', 'Vigente',
                                                                                         '16/01/2018', '6703',
                                                                                         'Estudio Abog.'],
                          ['20/02/2018', '20/02/2018', '3.27', '1.26', '1.68', '0', '0.33', 'Vigente', '20/02/2018',
                           '6918', 'Estudio Abog.'],
                          ['20/02/2018', '20/02/2018', '0.08', '0.03', '0.04', '0', '0.01', 'Vigente', '20/02/2018',
                           '6927', 'Estudio Abog.'],
                          ['09/03/2018', '10/03/2018', '2305', '910.22', '1167.86', '0', '226.92', 'Vigente',
                           '10/03/2018', '7030', 'Estudio Abog.'],
                          ['22/05/2018', '23/05/2018', '1150', '446.42', '589.4', '0', '114.18', 'Vigente',
                           '23/05/2018', '7371', 'Estudio Abog.'],
                          ['11/07/2018', '12/07/2018', '1150', '397.79', '591.32', '0', '160.89', 'Vigente',
                           '12/07/2018', '7522', 'Estudio Abog.'],
                          ['11/09/2018', '12/09/2018', '1150', '387.14', '602.26', '0', '160.6', 'Vigente',
                           '12/09/2018', '7705', 'Estudio Abog.']]


def test_refinancing():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/refinanciado_caido.html").read()
    r = HTML(html=page)
    assert r.search('Refinanciado: <B> * {} *')[0] == 'CAIDO'


def test_refinanciacion_propuesta():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/refinanciacion_propuesta.html").read()
    r = HTML(html=page)
    data_table = r.find('.Consulta')[1]
    table_titles = data_table.find('tr')[0]
    table_values = data_table.find('tr')[1]
    titles = table_titles.find('th')
    values = table_values.find('td')

    titles_list = []
    values_list = []

    for title in titles:
        titles_list.append(title.text)

    for value in values:
        values_list.append(value.text)

    assert titles_list == ['Cuota', 'Fecha Vto.', 'Total Cuota', 'Capital', 'Intereses', 'Impuestos', 'Honorarios',
                           'Condonable']
    assert values_list == ['1', '06/01/2020', '650,90', '149,44', '340,88', '87.04', '73.55', '']


def test_refinanciaciones_activa():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/refinanciaciones_activas.html").read()
    r = HTML(html=page)
    table = r.find('.tabla')[5]
    table_body = table.find('tr')
    basic_table_titles = []
    basic_table_values = []
    values = []
    for index, element in enumerate(table_body):
        if index == 0:
            titles = element.find('th')
        else:
            table_td = element.find('td')
            values.append(table_td)

    for title in titles:
        basic_table_titles.append(title.text)
    for value in values:
        subvalues = []
        for subvalue in value:
            subvalues.append(subvalue.text)
        basic_table_values.append(subvalues)

    assert basic_table_titles == ['Fecha', 'Total', 'Saldo', 'Cuotas', 'Cuota Original', 'Pago Fácil', 'Estado',
                                  'Tipo Ref']
    assert basic_table_values == [['20/01/2017', '0', '0', '18', '0', 'Sí', 'Anulada', 'E'],
                                  ['08/01/2018', '17297.02', '17297.02', '36', '1236.31', 'Sí', 'Activa', 'E']]

    table = r.find('.tabla')[7]
    table_body = table.find('tr')
    deep_table_titles = []
    deep_table_values = []
    values = []
    for index, element in enumerate(table_body):
        if index == 0:
            titles = element.find('th')
        else:
            table_td = element.find('td')
            values.append(table_td)
    for title in titles:
        deep_table_titles.append(title.text)

    for value in values:
        subvalues = []
        for subvalue in value:
            subvalues.append(subvalue.text)
        deep_table_values.append(subvalues)

    assert deep_table_titles == ['Cuota', 'Fecha Vto.', 'Capital', 'Intereses', 'Honorarios', 'Total Cuota',
                                 'Saldo Cuota', 'Condonable']
    assert deep_table_values == [['1', '22/01/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['2', '22/02/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['3', '22/03/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['4', '23/04/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['5', '22/05/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['6', '22/06/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['7', '23/07/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['8', '22/08/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['9', '22/09/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['10', '22/10/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['11', '22/11/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['12', '22/12/2018', '0', '0', '0', '0', '0', 'N'],
                                 ['13', '22/01/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['14', '22/02/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['15', '22/03/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['16', '22/04/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['17', '22/05/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['18', '22/06/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['19', '22/07/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['20', '22/08/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['21', '23/09/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['22', '22/10/2019', '0', '0', '0', '0', '0', 'N'],
                                 ['23', '22/11/2019', '387,7', '718,47', '119,18', '1225,35', '1225,35', 'N'],
                                 ['24', '23/12/2019', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['25', '22/01/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['26', '22/02/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['27', '23/03/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['28', '22/04/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['29', '22/05/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['30', '22/06/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['31', '22/07/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['32', '22/08/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['33', '22/09/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['34', '22/10/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['35', '23/11/2020', '394,3', '722,06', '119,92', '1236,28', '1236,28', 'N'],
                                 ['36', '22/12/2020', '394,33', '722,06', '119,92', '1236,31', '1236,31', 'N']]


def test_emision_refinanciacion():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/emision_refinanciacion.html").read()
    r = HTML(html=page)
    new_html = r.html.replace('img_new_style', 'https://abogados.naranja.com/img_new_style')
    new_html = new_html.replace('Resize_Image.aspx', 'https://abogados.naranja.com/Resize_Image.aspx')

    # print(new_html[54310:54398])
    # print(new_html[55916:56073])
    assert new_html[54310:54398] == "<img src=\"https://abogados.naranja.com/img_new_style/png/NAR.png\" width=\"30\"" \
                                    " height=\"30\""
    assert new_html[55916:56067] == "<img src=\"https://abogados.naranja.com/Resize_Image.aspx?IptFl=/abogados/" \
                                    "Temporarios/GestarAdjTmp/img_043400123628180229900036465840200000000000.png\"/>"


def test_refinanciacion_atrasada():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/refinanciado_activo.html").read()
    r = HTML(html=page)
    refinancing = r.search('Refinanciado: <B> * {} *')[0]
    assert refinancing == 'ACTIVO'
    expiring_date_string = r.search('Vto.: {}</a></td>')[0]
    expiring_date_obj = datetime.datetime.strptime(expiring_date_string, '%d/%m/%Y').date()
    today = datetime.datetime.now().date()
    if refinancing == 'ACTIVO':
        if today > expiring_date_obj:
            refinancing = 'ATRASADO'
        else:
            refinancing = 'AL DÍA'
    assert refinancing == 'ATRASADO'

    raw_expiring_date_string = '22/12/2019'
    expiring_date_obj = datetime.datetime.strptime(raw_expiring_date_string, '%d/%m/%Y').date()
    if today > expiring_date_obj:
        refinancing = 'ATRASADO'
    else:
        refinancing = 'AL DÍA'
    assert refinancing == 'AL DÍA'


def test_delete_refinancing():
    expiring_date = '11/01/2020'
    expiring_date_obj = datetime.datetime.strptime(expiring_date, '%d/%m/%Y').date()
    expiring_date_fmted = str(expiring_date_obj).replace('-', '')
    assert expiring_date_fmted == '20200111'


def test_customer_not_found():
    page = urllib.request.urlopen(
        "file:///home/pablo/lozadawebapp/tests/html_test_files/cliente_no_encontrado.html").read()
    r = HTML(html=page)
    no_found_message = r.search(';<b>{} con las condiciones solicitadas.')[0]
    assert no_found_message == 'No se encontraron clientes'

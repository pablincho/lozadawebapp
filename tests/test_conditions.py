from app.services import payment_conditions
from app.models import User, Conditions
import pytest
import datetime, locale


@pytest.fixture(name="admin_user")
def fixture_admin_user(db):
    rv = User(username="admin", role_id=3)
    rv.password = "admin"
    db.session.add(rv)
    db.session.commit()
    return rv


@pytest.fixture(name="conditions")
def fixture_conditions(db):
    condition1 = Conditions(code="a0600019", max_discount=True, installments_number=6, expiration_date=30)
    db.session.add(condition1)

    condition2 = Conditions(code="a0600163", max_discount=True, installments_number=6, expiration_date=30)
    db.session.add(condition2)

    condition3 = Conditions(code="a0600498", max_discount=True, installments_number=6, expiration_date=30)
    db.session.add(condition3)

    condition4 = Conditions(code="a0601141", assigned_time_less_than=10, total_debt_greater_than=20000,
                            interests_discount=50, installments_number=3, expiration_date=10)
    db.session.add(condition4)

    condition5 = Conditions(code="a0601141", assigned_time_less_than=30, assigned_time_greater_than=10,
                            total_debt_greater_than=20000, interests_discount=100, installments_number=3,
                            expiration_date=10)
    db.session.add(condition5)

    condition6 = Conditions(code="a0601141", assigned_time_less_than=60, assigned_time_greater_than=30,
                            total_debt_greater_than=20000, interests_discount=100, capital_discount=30,
                            installments_number=2, expiration_date=10)
    db.session.add(condition6)

    condition7 = Conditions(code="a0601141", assigned_time_less_than=100, assigned_time_greater_than=60,
                            total_debt_greater_than=20000, installments_number=1, expiration_date=10)
    db.session.add(condition7)

    condition8 = Conditions(code="a0601141", assigned_time_greater_than=100, total_debt_greater_than=20000,
                            installments_number=6, expiration_date=30)
    db.session.add(condition8)

    condition9 = Conditions(code="a0601134", assigned_time_less_than=10, total_debt_greater_than=20000,
                            interests_discount=50, installments_number=4, expiration_date=10)
    db.session.add(condition9)

    condition10 = Conditions(code="a0601134", assigned_time_greater_than=10, assigned_time_less_than=30,
                             total_debt_greater_than=20000, interests_discount=100, installments_number=3,
                             expiration_date=10)
    db.session.add(condition10)

    condition11 = Conditions(code="a0601134", assigned_time_greater_than=30, assigned_time_less_than=60,
                             total_debt_greater_than=20000, interests_discount=100, capital_discount=10,
                             installments_number=3, expiration_date=10)
    db.session.add(condition11)

    condition12 = Conditions(code="a0601134", assigned_time_greater_than=60, assigned_time_less_than=120,
                             total_debt_greater_than=20000, max_discount=True, installments_number=2,
                             expiration_date=10)
    db.session.add(condition12)

    condition13 = Conditions(code="a0601134", assigned_time_greater_than=120, total_debt_greater_than=20000,
                             max_discount=True, installments_number=6,
                             expiration_date=30)
    db.session.add(condition13)

    condition14 = Conditions(code="a0601134", total_debt_greater_than=10000, total_debt_less_than=20000,
                             installments_number=3, expiration_date=10)
    db.session.add(condition14)

    condition15 = Conditions(code="a0601134", total_debt_less_than=10000, installments_number=2, expiration_date=10)
    db.session.add(condition15)

    db.session.commit()
    # return conditions


def test_payment_conditions1(test_client, db, conditions):
    payment_data = {
        'total_debt': '18328,10',
        'expiring_date': '09/02/2020',
        'max_cuotas_quita': '6',
        'txt_CNTx_CQ_field': '_Cod:1Seleccion',
        'vto_primera_cuota': '09/02/2020',
        'vto_cada': '30',
        'vto_dias': '5',
        'address': 'ALBATROS 27 00505',
        'min_payment': '5779,67'
    }
    today = datetime.date.today()
    server_username = 'a0600019'
    risk = 'NAR-ALTO'
    assignment_date = '28/07/2018'
    capital = '4510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                                        int_punit, int_finan)
    assert payment_amount == payment_data['min_payment']
    assert installments_number == 6
    assert expiring_date == str(today + datetime.timedelta(days=30))
    assert discount_exist is True


def test_payment_conditions2(test_client, db, conditions):
    payment_data = {
        'total_debt': '21090,10',
        'expiring_date': '09/02/2020',
        'max_cuotas_quita': '6',
        'txt_CNTx_CQ_field': '_Cod:1Seleccion',
        'vto_primera_cuota': '09/02/2020',
        'vto_cada': '30',
        'vto_dias': '5',
        'address': 'ALBATROS 27 00505',
        'min_payment': '5779,67'
    }
    today = datetime.date.today()
    server_username = 'a0601141'
    risk = 'NAR-ALTO'
    assignment_date = today - datetime.timedelta(days=20)
    assignment_date = assignment_date.strftime('%d/%m/%Y')
    capital = '4510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                                        int_punit, int_finan)
    assert payment_amount == payment_data['min_payment']
    assert installments_number == 3
    assert expiring_date == str(today + datetime.timedelta(days=10))
    assert discount_exist is True

    capital = '6510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                                        int_punit, int_finan)
    assert payment_amount == '7692,21'
    assert installments_number == 3
    assert expiring_date == str(today + datetime.timedelta(days=10))
    assert discount_exist is True


def test_payment_conditions3(test_client, db, conditions):
    payment_data = {
        'total_debt': '21090,10',
        'expiring_date': '09/02/2020',
        'max_cuotas_quita': '6',
        'txt_CNTx_CQ_field': '_Cod:1Seleccion',
        'vto_primera_cuota': '09/02/2020',
        'vto_cada': '30',
        'vto_dias': '5',
        'address': 'ALBATROS 27 00505',
        'min_payment': '5779,67'
    }
    today = datetime.date.today()
    server_username = 'a0601141'
    risk = 'NAR-ALTO'
    assignment_date = today - datetime.timedelta(days=40)
    assignment_date = assignment_date.strftime('%d/%m/%Y')
    capital = '4510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                                        int_punit, int_finan)
    assert payment_amount == payment_data['min_payment']
    assert installments_number == 2
    assert expiring_date == str(today + datetime.timedelta(days=10))
    assert discount_exist is True

    capital = '7510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                            int_punit, int_finan)
    assert payment_amount == '6211,6'
    assert installments_number == 2
    assert expiring_date == str(today + datetime.timedelta(days=10))
    assert discount_exist is True


def test_payment_conditions4(test_client, db, conditions):
    payment_data = {
        'total_debt': '21090,10',
        'expiring_date': '09/02/2020',
        'max_cuotas_quita': '6',
        'txt_CNTx_CQ_field': '_Cod:1Seleccion',
        'vto_primera_cuota': '09/02/2020',
        'vto_cada': '30',
        'vto_dias': '5',
        'address': 'ALBATROS 27 00505',
        'min_payment': '5779,67'
    }
    today = datetime.date.today()
    server_username = 'a0601141'
    risk = 'NAR-ALTO'
    assignment_date = today - datetime.timedelta(days=70)
    assignment_date = assignment_date.strftime('%d/%m/%Y')
    capital = '4510,55'
    int_punit = '1229,45'
    int_finan = '493,10'
    payment_amount, installments_number, expiring_date, discount_exist = \
        payment_conditions.apply_conditions(payment_data, server_username, assignment_date, risk, capital,
                                                        int_punit, int_finan)
    assert payment_amount == '21090,10'
    assert installments_number == 1
    assert expiring_date == str(today + datetime.timedelta(days=10))
    assert discount_exist is False

import pytest
from app import create_app, db as _db
from app.models import Role


@pytest.fixture(scope="session")
def session_app():
    app = create_app()
    return app


@pytest.fixture(name="app")
def fixture_app(session_app):
    """Establish an application context before running the tests."""
    app = session_app
    _ctx = app.test_request_context()
    _ctx.push()
    yield app

    _ctx.pop()


@pytest.fixture
def db(app):
    with app.app_context():
        _db.create_all()
        Role.insert_roles()
    yield _db
    _db.session.close()
    _db.drop_all()


@pytest.fixture
def client(app):
    with app.test_client() as c:
        yield c


@pytest.fixture(scope="module")
def test_client():
    flask_app = create_app()
    testing_client = flask_app.test_client()

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()

from app.models import User, ExcelContents
import pytest
from app.services import from_file_to_db
from pathlib import Path
import time
from sqlalchemy import desc


@pytest.fixture(name="admin_user")
def fixture_admin_user(db):
    rv = User(username="admin", role_id=3)
    rv.password = "admin"
    db.session.add(rv)
    db.session.commit()
    return rv


def test_read_excel(db, app):
    data_folder = Path("excel_test_files/")
    file_to_open = data_folder / "gestion.xlsx"

    with open(file_to_open, 'rb') as file:
        data = file.read()
    start = time.time()
    from_file_to_db.insert_excel_info(data)
    elapsed = time.time() - start
    print(elapsed)
    excel = db.session.query(ExcelContents).filter_by(operador='Pilar').first()
    assert excel.operador == 'Pilar'


def test_read_csv(db, app):
    data_folder = Path("excel_test_files/")
    file_to_open = data_folder / "gestion.csv"

    with open(file_to_open, 'rb') as file:
        data = file.read()
    start = time.time()
    from_file_to_db.insert_csv_info(data)
    elapsed = time.time() - start
    print(elapsed)
    excel = db.session.query(ExcelContents).filter_by(operador='Pilar').first()
    assert excel.operador == 'Pilar'
    from_file_to_db.insert_csv_info(data)
    elapsed = time.time() - start
    print(elapsed)
    excel = db.session.query(ExcelContents).filter_by(operador='Pilar').first()
    assert excel.operador == 'Pilar'


def test_query_distinct(db, app):
    data_folder = Path("excel_test_files/")
    file_to_open = data_folder / "gestion.csv"

    with open(file_to_open, 'rb') as file:
        data = file.read()
    from_file_to_db.insert_csv_info(data)
    # count1 = db.session.query(ExcelContents.codigo).filter_by(operador='Julieta').count()
    # count2 = db.session.query(ExcelContents.codigo).count()
    # distincts1 = db.session.query(ExcelContents.codigo).filter_by(operador='Julieta').distinct()
    # distincts2 = db.session.query(ExcelContents.codigo).distinct()
    query = db.session.query(ExcelContents).filter_by(operador='Julieta').order_by(desc(ExcelContents.codigo)).all()
    # ordered_query = query.order_by(ExcelContents.operador)
    for element in query:
        print(element.codigo)
    # assert excel

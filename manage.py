import pytz
from app import create_app
from flask_migrate import Migrate
from app import db
from datetime import datetime

app = create_app()

migrate = Migrate(app, db)

@app.template_filter('format_datetime')
def format_datetime(value):
    """Format a datetime object as DD-MM-YYYY HH:MM."""
    if value is None:
        return ""
    local_tz_value = value.astimezone(pytz.timezone("America/Argentina/Cordoba"))
    return local_tz_value.strftime("%d-%m-%Y %H:%M")

@app.template_filter('format_date')
def format_date(value):
    """Format a datetime object as DD-MM-YYYY."""
    if isinstance(value, datetime):
        return value.strftime("%d-%m-%Y")
    elif isinstance(value, str):
        date_str = value.split(' ')[0]
        return date_str
    else:
        return ""

@app.template_filter('format_inverted_date')
def format_inverted_date(value):
    """Format a datetime object as DD-MM-YYYY."""
    if isinstance(value, datetime):
        return value.strftime("%d-%m-%Y")
    elif isinstance(value, str):
        date_str = value.split(' ')[0]
        if "-" in date_str:
            date_list = date_str.split("-")
        else:
            date_list = date_str.split("/")
        date_str = "-".join([date_list[2], date_list[1], date_list[0]])
        return date_str
    else:
        return ""
# Register the filter
app.jinja_env.filters['format_datetime'] = format_datetime
app.jinja_env.filters['format_date'] = format_date
app.jinja_env.filters['format_inverted_date'] = format_inverted_date
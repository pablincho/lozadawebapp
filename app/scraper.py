from requests_html import HTMLSession
import datetime
from flask import session as flask_session
import requests


class Scraper:
    URL_NARANJA ='https://abogados.naranjax.com'
    def __init__(self):
        self.client_code = ''
        self.nro_producto = ''
        self.inicio_mora = ''
        self.session = HTMLSession()

    def login(self, username, password):
        payload = {
            'Codigo': username,
            'Contrasenia': password,
            'CambiarPass': '0'
        }

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,'
                      'application/signed-exchange;v=b3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'es-AR,es;q=0.9,es-419;q=0.8,en;q=0.7',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            # 'Host': 'abogados.naranjax.com',
            'Origin': self.URL_NARANJA
        }
        r = self.session.post(f'{self.URL_NARANJA}/AbogadoValida.asp', data=payload, headers=headers,
                              allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        else:
            wrong_user_or_pass = r.html.find('[value="No coinciden el usuario y la clave."]')
            if wrong_user_or_pass:
                return False

            my_cookies = requests.utils.dict_from_cookiejar(r.cookies)
            flask_session['cookies'] = my_cookies
            return True

    def logout(self):
        self.session.get(f'{self.URL_NARANJA}/TNSalir.asp')
        return

    def get_basic_data(self, dni):
        # Insert DNI and get basic data
        insert_dni_payload = {
            'Apellido': '',
            'Nombre': '',
            'TipoDocumento': 'DU',
            'NroDocumento': dni
            # 'Submit': 'Buscar...'
        }

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Encoding': 'gzip, deflate, br, zstd',
            'Accept-Language': 'es-ES,es;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'abogados.naranjax.com',
            'Origin': self.URL_NARANJA,
            'Referer': f'{self.URL_NARANJA}/ContactosBusca.asp',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36'
        }
        try:
            my_cookies = flask_session['cookies']
        except KeyError:
            return False
        r = self.session.post(f'{self.URL_NARANJA}/Contactos.asp', data=insert_dni_payload, headers=headers,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        try:
            no_found_message = r.html.search(';<b>{} con las condiciones solicitadas.')[0]
            if no_found_message == 'No se encontraron clientes':
                return 'no user'
        except TypeError:
            pass

        self.client_code = r.html.search("<script>selCliente('{}');</script>")[0]
        code_payload = {
            'Codigo': self.client_code
        }
        r = self.session.post(f"{self.URL_NARANJA}/Contacto.asp", data=code_payload, cookies=my_cookies, verify=False)
        last_name = r.html.find('#Apellido_contacto', first=True)

        name = r.html.find('#Nombre_contacto', first=True)

        dni = r.html.find('.destacado')[1]

        self.nro_producto = r.html.search("'NAR ','{}   ',")[0]

        self.inicio_mora = r.html.search("   ','{}');\">Naranja")[0]

        tabla_sucursal = r.html.find('.tabla')[0]
        sucursal = tabla_sucursal.find('td')[9]

        # See refinancing
        code_payload = {
            'Codigo': self.client_code,
            'Abogado': '',
            'Producto': 'NAR',
            'NroProducto': self.nro_producto,
            'InicioMora': self.inicio_mora
        }
        r = self.session.post(f"{self.URL_NARANJA}/Producto.asp", data=code_payload, cookies=my_cookies, verify=False)
        r.html.encoding = 'ISO-8859-1'
        try:
            refinancing = r.html.search('Refinanciado: <B> * {} *')[0]
            expiring_date_string = r.html.search('Vto.: {}</a></td>')[0]
            expiring_date_obj = datetime.datetime.strptime(expiring_date_string, '%d/%m/%Y').date()
            today = datetime.datetime.now().date()
            if refinancing == 'ACTIVO':
                if today > expiring_date_obj:
                    refinancing = 'ATRASADO'
                else:
                    refinancing = 'AL DÍA'
        except TypeError:
            refinancing = None

        risk = r.html.find('#Riesgo_contacto', first=True)
        padron = r.html.search('Padrón\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        capital = r.html.search('Capital\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        int_finan = r.html.search('Int. Financieros \r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        int_punit = r.html.search('Int. Punitorios\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        iva_intereses = r.html.search('IVA Intereses\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        honorarios = r.html.search('Honorarios\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        iva_honorarios = r.html.search('IVA Honorarios\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        imp_sellos = r.html.search('Impuesto Sellos\r\n</td>\r\n<td class="Dato">\r\n{}\r\n</td>')[0]
        total_deuda = r.html.search('Total Deuda\r\n</td>\r\n<td class="Dato">\r\n<font class=\'destacado\'><b>{}</b>')[
            0]

        return {
            'last_name': last_name.text,
            'name': name.text,
            'dni': dni.text,
            'company': 'TARJETA NARANJA SA',
            'office': sucursal.text,
            'risk': risk.text,
            'assignment_date': padron,
            'nro_producto': self.nro_producto,
            'inicio_mora': self.inicio_mora,
            'client_code': self.client_code,
            'refinancing': refinancing,
            'capital': capital,
            'int_finan': int_finan,
            'int_punit': int_punit,
            'iva_intereses': iva_intereses,
            'honorarios': honorarios,
            'iva_honorarios': iva_honorarios,
            'imp_sellos': imp_sellos,
            'total_deuda': total_deuda
        }

    def get_payment_data(self, client_code, nro_producto, inicio_mora):
        # Pagos
        code_payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'OtrasEmp': ''
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f"{self.URL_NARANJA}/ProductoPagos.asp", data=code_payload, cookies=my_cookies,
                              allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        total_debt = \
            r.html.search("Total Deuda\r\n</td>\r\n<td class=\"Dato\">\r\n<font class='destacado'><b>{}</b></font>")[0]

        payments_data_search = r.html.search_all("'ProductoPagoDetalle.asp','{}');\">")

        payment_date = []
        for data in payments_data_search:
            payment_date.append(data.fixed[0])

        payments_table = r.html.find("#tblPagos")[0]

        table_list = []

        table_body = payments_table.find("tr")
        for index, table_row in enumerate(table_body):
            element_list = []
            if index == 1:
                table_th = table_row.find('th')
                for element in table_th:
                    element_list.append(element.text)

            elif index > 1:
                table_td = table_row.find('td')
                for element in table_td:
                    element_list.append(element.text)

            table_list.append(element_list)

        return {
            'table': table_list,
            'payment_date': payment_date,
            'total_debt': total_debt
        }

    def get_payment_detail(self, client_code, nro_producto, inicio_mora, payment_date):
        # Get payments
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'FechaPago': payment_date
        }
        my_cookies = flask_session['cookies']
        r = self.session.get(f'{self.URL_NARANJA}/ProductoPagoDetalle.asp', data=payload, cookies=my_cookies,
                             allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False

        formatted_payment_date = r.html.search("Fecha Pago\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        status = r.html.search("Estado\r\n</td>\r\n<td class=\"Dato\">\r\n<font class='destacado'><b>{}</b></font>")[0]
        arqueo_date = r.html.search("Fecha Arqueo\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        receipt_number = r.html.search("Nro. Recibo\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        payment_type = r.html.search("Tipo de Pago\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        amount = r.html.search("Monto en pesos\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        capital = r.html.search("Capital\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        finance_interest = \
            r.html.search("Int. Financ. y Gastos Emp.\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        lawyer_cost = r.html.search("Gastos Abogado\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        punit_interest = r.html.search("Int. Punitorios\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        iva_interest = r.html.search("IVA Intereses\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        fees = r.html.search("Honorarios\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        fee_taxes = r.html.search("IVA Honorarios\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        seals_taxes = r.html.search("Impuesto Sellos\r\n</td>\r\n<td class=\"Dato\">\r\n{}\r\n</td>\r\n</tr>")[0]
        total = r.html.search("Total\r\n</td>\r\n<td class=\"Dato\">\r\n<font class='destacado'><b>{}</b></font>")[0]

        return {
            'payment_date': payment_date,
            'formatted_payment_date': formatted_payment_date,
            'status': status,
            'arqueo_date': arqueo_date,
            'receipt_number': receipt_number,
            'payment_type': payment_type,
            'amount': amount,
            'capital': capital,
            'finance_interest': finance_interest,
            'lawyer_cost': lawyer_cost,
            'punit_interest': punit_interest,
            'iva_interest': iva_interest,
            'fees': fees,
            'fee_taxes': fee_taxes,
            'seals_taxes': seals_taxes,
            'total': total
        }

    def get_payment_receipt(self, client_code, nro_producto, inicio_mora, payment_date):
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'FechaPago': payment_date
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/EmisionRecibo.asp', data=payload,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        if not r.text:
            return ' '
        else:
            return r.text

    def get_new_payment(self, client_code, nro_producto, inicio_mora):
        code_payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'OtrasEmp': ''
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f"{self.URL_NARANJA}/ProductoPagos.asp", data=code_payload,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False

        total_debt_raw = \
            r.html.search("Total Deuda\r\n</td>\r\n<td class=\"Dato\">\r\n<font class='destacado'><b>{}</b></font>")[0]
        total_debt = total_debt_raw.replace('.', '')
        min_payment_raw = r.html.search(
            "<TR>\r\n\t\t\t\t\r\n\t\t\t\t<TD ALIGN=\'CENTER\' CLASS=\'DATO\'><B>{}</B></TD>\r\n\t\t\t\t<TD ALIGN=\'CENTER\' CLASS=\'DATO\'><B>")[
            0]
        min_payment = min_payment_raw.replace('.', '')
        try:
            cuotas_quita = r.html.search('"quitaEnCuotas({},')[0]
        except TypeError:
            cuotas_quita = ''

        txt_CNTx_CQ_field = r.html.search('txt_CNTx_CQ.value\t\t= "{}";\r\n\t\t\t\t\r\n\t\t\t')[0]

        code_payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'Fecha': datetime.datetime.now().strftime("%d/%m/%y"),
            'CheckPosnet_PAGO': '',
            'NroTarjeta_PAGO': '',
            'NroTrans_PAGO': '',
            'Nrocupon_PAGO': '',
            'txtMaXCUOTASQUITA': cuotas_quita,                # Máxima cantidad de cuotas permitida por Tarjeta Naranja
            'txt_CNTx_CQ': txt_CNTx_CQ_field,
            'txtValorQuitaTotal': min_payment,                # Mínimo valor permitido a pagar (Monto mínimo)
            'txtValorOriginal': total_debt,                     # Deuda TOTAL (no siempre es el monto a pagar)
            'MontoPesos': total_debt,                           # Monto a pagar (no siempre es la deuda total) siempre debe ser mayor al campo txtValorQuitaTotal
            'Moneda1': '',
            'MontoMoneda1': '0',
            'Moneda2': '',
            'MontoMoneda2': '0'
        }

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
                      ',application/signed-exchange;v=b3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'es-AR,es;q=0.9,es-419;q=0.8,en;q=0.7',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        r = self.session.post(f"{self.URL_NARANJA}/ProductoRefActualiza.asp", data=code_payload,
                              headers=headers, cookies=my_cookies, verify=False)
        expiring_date = r.html.search(
            'Primera Cuota (dd/mm/yyyy)\r\n</td>\r\n<td class="Dato"><input type="text" value="{}" name="VtoPrimeraCuota"')[
            0]

        vto_primera_cuota = r.html.find('[name=VtoPrimeraCuota]', first=True).attrs['value']
        vto_cada = r.html.find('[name=VtoCada]', first=True).attrs['value']
        vto_dias = r.html.find('[name=VtoDias]', first=True).attrs['value']
        address = r.html.find('[name=Domicilio]', first=True).attrs['value']

        return {
            'total_debt': total_debt,
            'total_debt_raw': total_debt_raw,
            'expiring_date': expiring_date,
            'max_cuotas_quita': cuotas_quita,
            'txt_CNTx_CQ_field': txt_CNTx_CQ_field,
            'vto_primera_cuota': vto_primera_cuota,
            'vto_cada': vto_cada,
            'vto_dias': vto_dias,
            'address': address,
            'min_payment': min_payment
        }

    def get_refinancing_info(self, client_code, nro_producto, inicio_mora, total_debt, max_cuotas_quita,
                             txt_CNTx_CQ_field, username, vto_primera_cuota, vto_cada, vto_dias, address,
                             installments_number, payment_amount):
        code_payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'MontoPesos': payment_amount,                                   # Monto a pagar (no siempre es la deuda total) siempre debe ser mayor al campo txtValorQuitaTotal
            'txtMaXCUOTASQUITA': max_cuotas_quita,                          # Máxima cantidad de cuotas permitida por Tarjeta Naranja
            'txt_CNTx_CQ': txt_CNTx_CQ_field,
            'txtEMPRESA': 'NAR',
            'txtTipoRefinanciacion': 'Q',
            'txtNroDocCliente': username,
            'txtTasaMinima': '0.00',
            'txtTasaMaxima': '0.00',
            'EsReformulacion': '',
            'hdnTotalDeuda': total_debt,                                # Deuda TOTAL (no siempre es el monto a pagar)
            'TipoCalculo': '0',
            'CantCuotas': installments_number,                          # Cantidad de cuotas elegidas a pagar (siempre debe ser menor al campo txtMaXCUOTASQUITA
            'VtoPrimeraCuota': vto_primera_cuota,                       # Vencimiento elegido para pagar
            'VtoCada': vto_cada,
            'VtoDias': vto_dias,
            'Tasa': '0.00',
            'Domicilio': address,
            'TipoPagoFacil': '1'
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f"{self.URL_NARANJA}/ProductoRefConsulta.asp", data=code_payload,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False

        data_table = r.html.find('.Consulta')[1]
        titles_list = []
        values_list = []
        sub_values_list = []

        table_rows = data_table.find('tr')
        table_rows.pop()
        for index, row in enumerate(table_rows):
            if index == 0:
                titles = row.find('th')
                for title in titles:
                    titles_list.append(title.text)
            else:
                values = row.find('td')
                for value in values:
                    sub_values_list.append(value.text)
                values_list.append(sub_values_list)
                sub_values_list = []
        return {
            'titles_list': titles_list,
            'values_list': values_list
        }

    def get_active_refinancing_data(self, client_code, nro_producto, inicio_mora):
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'OtrasEmp': ''
        }
        headers = {
            "Accept-Encoding": 'gzip, deflate, br',
            "Accept-Language": 'es-AR,es;q=0.9,es-419;q=0.8,en;q=0.7',
            "Accept": 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,'
                      'application/signed-exchange;v=b3'
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/ProductoRefinanciaciones.asp', data=payload,
                                 headers=headers, cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        r.html.encoding = 'ISO-8859-1'
        table = r.html.find('.tabla')[5]
        table_body = table.find('tr')
        basic_table_titles = []
        basic_table_values = []
        values = []
        for index, element in enumerate(table_body):
            if index == 0:
                titles = element.find('th')
            else:
                table_td = element.find('td')
                values.append(table_td)
        for title in titles:
            basic_table_titles.append(title.text)
        for value in values:
            subvalues = []
            for subvalue in value:
                subvalues.append(subvalue.text)
            basic_table_values.append(subvalues)
        table = r.html.find('.tabla')[7]
        table_body = table.find('tr')
        deep_table_titles = []
        deep_table_values = []
        values = []
        for index, element in enumerate(table_body):
            if index == 0:
                titles = element.find('th')
            else:
                table_td = element.find('td')
                values.append(table_td)
        for title in titles:
            deep_table_titles.append(title.text)

        for value in values:
            subvalues = []
            for subvalue in value:
                subvalues.append(subvalue.text)
            deep_table_values.append(subvalues)

        return {
            'basic_table_titles': basic_table_titles,
            'basic_table_values': basic_table_values,
            'deep_table_titles': deep_table_titles,
            'deep_table_values': deep_table_values
        }

    def get_refinancing_receipt(self, client_code, nro_producto, inicio_mora):
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'Cuotas': '',
            'PagoFacil': '',
            'EsReformulacion': None
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/EmisionRefinanciacion.asp', data=payload,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        new_html = r.text.replace('img_new_style', f'{self.URL_NARANJA}/img_new_style')
        new_html = new_html.replace('Resize_Image.aspx', f'{self.URL_NARANJA}/Resize_Image.aspx')
        return new_html

    def update_new_refinancing(self, client_code, nro_producto, inicio_mora, vto_cada, vto_dias, vto_primera_cuota,
                               address, max_cuotas_quita, txt_CNTx_CQ_field, installments_number,
                               payment_amount):
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'TipoCalculo': '0',
            'CantCuotas': installments_number,
            'Cuotas': installments_number,
            'VtoCada': vto_cada,
            'VtoDias': vto_dias,
            'VtoPrimeraCuota': vto_primera_cuota,  # formato: '20200111',
            'Tasa': '0',
            'Domicilio': address,
            'TipoPagoFacil': '1',
            'cboCUIT_ID': None,
            'MontoPesos': payment_amount,
            'txtMaXCUOTASQUITA': max_cuotas_quita,
            'txt_CNTx_CQ': txt_CNTx_CQ_field,
            'txtCancelar': '1',
            'EsReformulacion': None,
            'EntregaReformulacionPesos': '0',
            'MontoMoneda1': '0',
            'MontoMoneda2': '0',
            'Moneda1': None,
            'Moneda2': None
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/ProductoRefGraba.asp', data=payload, cookies=my_cookies,
                              allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        else:
            return True

    def get_refinancing_after_created(self, client_code, nro_producto, inicio_mora):
        today = str(datetime.datetime.now().date())

        payload = {
            'txtMarcaAgua': 'TRUE',
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'FechaPago': today,
            'Cuotas': '1',
            'PagoFacil': '1',
            'EsReformulacion': 'true',
            'MostrarMsj': None,
            'EmitirRecibo': '0'
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/EmisionRefinanciacion.asp', data=payload,
                                 cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        new_html = r.text.replace('img_new_style', f'{self.URL_NARANJA}/img_new_style')
        new_html = new_html.replace('Resize_Image.aspx', f'{self.URL_NARANJA}/Resize_Image.aspx')
        return new_html

        # payload = {
        #     'txtMarcaAgua': 'TRUE',
        #     'Codigo': client_code,
        #     'Producto': 'NAR',
        #     'NroProducto': nro_producto,
        #     'InicioMora': inicio_mora,
        #     'FechaPago': '????',
        # }
        #
        # r = Scraper.session.post(f'{self.URL_NARANJA}/Producto.asp', data=payload,
        #                          allow_redirects=True)
        # if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
        #     return False

    def delete_refinancing(self, client_code, nro_producto, inicio_mora):
        # payload = {
        #     'Codigo': client_code,
        #     'Producto': 'NAR',
        #     'NroProducto': nro_producto,
        #     'InicioMora': inicio_mora,
        #     'Cuotas': '',
        #     'PagoFacil': '',
        #     'EsReformulacion': None
        # }
        # r = Scraper.session.post(f'{self.URL_NARANJA}/ProductoRefConfirmaAnulacion.asp', data=payload,
        #                          allow_redirects=True)
        # if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
        #     return False
        #
        # print(r.text)
        payload = {
            'Codigo': client_code,
            'Producto': 'NAR',
            'NroProducto': nro_producto,
            'InicioMora': inicio_mora,
            'ImpSellos': '0'
        }
        my_cookies = flask_session['cookies']
        r = self.session.post(f'{self.URL_NARANJA}/ProductoRefAnulacion.asp', data=payload,
                              cookies=my_cookies, allow_redirects=True, verify=False)
        if r.url == f'{self.URL_NARANJA}/login_abogados.asp':
            return False
        else:
            return True


if __name__ == '__main__':
    scraper = Scraper()
    # scraper.get_basic_data()
    # scraper.get_payment_data('3363979', 'DU00025469313', '20160210')
    # scraper.get_payment_detail('3363979', 'DU00025469313', '20160210', '20170707161323')
    # scraper.get_new_payment('3363979', 'DU00025469313', '20160210')
    scraper.get_refinancing_info('3363979', 'DU00025469313', '20160210', '150', '3', '_Cod:1Seleccion',
                                 '25469313', '05/01/2020', '30', '5', 'CARRA 00336')

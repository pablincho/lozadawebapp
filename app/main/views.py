from . import main
from app import db
from app.services.get_sheet_from_drive import get_sheet_and_save_to_db
from flask import render_template, redirect, request, flash, url_for, session, make_response, jsonify, send_file, current_app
from flask_login import current_user, login_user, logout_user, login_required
from app.forms import LoginForm, FileForm, ContactPhoneForm, ImageFileForm, ExcelFileForm
from app.models import User, ContactPhone, VisitCounter, RefinancingCounter, Conditions, FileContents
from sqlalchemy.orm.exc import NoResultFound
from app.scraper import Scraper
from app.server_passwords import get_server_password
import datetime, locale
from flask_weasyprint import HTML, render_pdf
from app.services import dbdict
import flask_excel as excel
import pytz
from app.services import from_file_to_db, visit_counter, payment_conditions
from app.services.args_dict import user_args
from app.services.tools import convert_two_digit_number
from io import BytesIO
from flask_paginate import Pagination, get_page_parameter
from webargs import fields
from webargs.flaskparser import use_args
from sqlalchemy import desc, asc
from datetime import datetime, timezone
from sqlalchemy import create_engine, select, MetaData, Table

locale.setlocale(locale.LC_NUMERIC, "es_ES.UTF-8")


@main.route('/')
@main.route('/index')
def index():
    contact_phone = db.session.query(ContactPhone).filter_by(id=1).one_or_none()
    if contact_phone:
        tel_contacto = contact_phone.phone
    else:
        tel_contacto = None
    return render_template("index.html", tel_contacto=tel_contacto)


@main.route('/antecedentes')
def antecedentes():
    return render_template("antecedentes.html")


@main.route('/clientes')
def clientes():
    return render_template("clientes.html")


@main.route('/abogados')
def abogados():
    return render_template("abogados.html")


@main.route('/areas_practicas')
def areas_practicas():
    return render_template("areas_practicas.html")


@main.route('/cobranzas')
def cobranzas():
    return render_template("cobranzas.html")


@main.route('/contacto')
def contacto():
    contact_phone = db.session.query(ContactPhone).filter_by(id=1).one_or_none()
    if contact_phone:
        tel_contacto = contact_phone.phone
    else:
        tel_contacto = None
    return render_template("contacto.html", tel_contacto=tel_contacto)


@main.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    contact_phone = db.session.query(ContactPhone).filter_by(id=1).one_or_none()
    if contact_phone:
        tel_contacto = contact_phone.phone
    else:
        tel_contacto = None

    if request.method == "GET":
        if current_user.is_authenticated:
            if current_user.is_admin or current_user.is_super:
                return redirect(url_for("main.users"))
            else:
                visit_counter.increase_visit_counter()
                return redirect(url_for("main.user_data"))
    if request.method == "POST":
        if form.validate_on_submit():
            user = db.session.query(User).filter_by(username=form.user.data).one_or_none()
            if user:
                if user.check_password(form.password.data):
                    login_user(user, form.remember_me.data)
                    if current_user.is_admin or current_user.is_super:
                        return redirect(url_for("main.users"))
                    elif current_user.is_normal:
                        visit_counter.increase_visit_counter()
                        if form.phone.data:
                            user.phone = form.phone.data
                        user.last_login = datetime.now(timezone.utc)
                        db.session.add(user)
                        db.session.commit()
                        return redirect(url_for("main.user_data"))

            flash("Nombre de usuario o contraseña incorrectos", "error")
            return render_template("login.html", form=form), 403

    return render_template("login.html", form=form, tel_contacto=tel_contacto)


@main.route("/logout")
@login_required
def logout():
    if current_user.is_normal:
        logout_scraper = Scraper()
        logout_scraper.logout()
    logout_user()
    flash("Usuario deslogueado", 'message')

    return redirect(url_for("main.login"))


@main.route("/upload", methods=["GET", "POST"])
@login_required
def upload_customers():
    if current_user.is_admin:
        form = FileForm()
        if request.method == "POST":
            if form.validate_on_submit():
                from_file_to_db.insert_users()
    else:
        return redirect(url_for('main.users'))
    return render_template("upload_file.html", form=form)


@main.route("/subir_excel", methods=["GET", "POST"])
@login_required
def upload_excel():
    form = ExcelFileForm()
    if request.method == "POST":
        if current_user.is_admin:
            if form.validate_on_submit():
                content = request.files['excel_file'].read()
                from_file_to_db.insert_csv_info(content)
                from_file_to_db.insert_date_csv_info(datetime.datetime.utcnow())
                flash("Archivo subido exitosamente", 'message')
        else:
            return redirect(url_for('main.users'))
    date_excel = db.session.query(ExcelInfoContents).first()
    date_time = None
    if date_excel:
        date_time = "{0}/{1}/{2} {3}:{4}".format(convert_two_digit_number(date_excel.date.day),
                                                 convert_two_digit_number(date_excel.date.month), date_excel.date.year,
                                                 convert_two_digit_number(date_excel.date.hour - 3),
                                                 convert_two_digit_number(date_excel.date.minute))

    return render_template("upload_excel.html", form=form, date=date_time)


@main.route("/usuarios")
@login_required
def users():
    page = request.args.get(get_page_parameter(), type=int, default=1)
    all_users = db.session.query(User).paginate(page=page, error_out=True)
    pagination = Pagination(page=page, per_page=all_users.per_page, total=all_users.total, search=False,
                            record_name='users')
    return render_template("usuarios.html", pagination=pagination, users=all_users, css_framework='bootstrap4')


@main.route("/usuarios/remover/<user_id>")
@login_required
def remove_user(user_id: int):
    """Removes a user"""
    user = db.session.query(User).filter_by(id=user_id).one()
    if current_user.is_admin:
        db.session.delete(user)
        db.session.commit()
        return redirect(url_for("main.users"))
    else:
        flash("You don't have permissions to delete this user.", "error")
        return redirect(url_for("main.users")), 403


@main.route("/usuarios/remover_todos")
@login_required
def remove_all_users():
    """Removes all users"""
    if current_user.is_admin:
        try:
            num_rows_deleted = db.session.query(User).filter_by(role_id=1).delete()
            db.session.commit()
            flash('{} usuarios eliminados'.format(num_rows_deleted), 'message')
        except:
            db.session.rollback()
        return redirect(url_for("main.users"))
    else:
        flash("You don't have permissions to delete this user.", "error")
        return redirect(url_for("main.users")), 403


@main.route("/telcontacto", methods=["GET", "POST"])
@login_required
def tel_contacto():
    if current_user.is_admin:
        form = ContactPhoneForm()
        if request.method == "GET":
            try:
                show_phone = db.session.query(ContactPhone).filter_by(id=1).first()
            except NoResultFound:
                show_phone = None
            if show_phone:
                contact_phone = show_phone.phone
            else:
                contact_phone = "No hay teléfono registrado"
        if request.method == "POST":
            if form.validate_on_submit():
                try:
                    show_phone = db.session.query(ContactPhone).filter_by(id=1).first()
                    if show_phone:
                        show_phone.phone = str(form.phone.data)
                        db.session.commit()
                    else:
                        show_phone = ContactPhone(phone=str(form.phone.data), id=1)
                        db.session.add(show_phone)
                        db.session.commit()
                except NoResultFound:
                    show_phone = ContactPhone(phone=str(form.phone.data), id=1)
                    db.session.add(show_phone)
                    db.session.commit()
                flash("Teléfono cambiado con éxito", "message")
                return redirect(url_for("main.tel_contacto"))
    else:
        return redirect(url_for('main.users'))
    return render_template("telcontacto.html", form=form, show_phone=contact_phone)


@main.route("/datos_usuario", methods=["GET", "POST"])
@login_required
def user_data():
    user_dni = current_user.username
    contact_phone = db.session.query(ContactPhone).filter_by(id=1).one_or_none()
    try:
        conn_string = current_app.config['SQLALCHEMY_DATABASE_URI']
        engine = create_engine(conn_string)
        metadata = MetaData()
        users_table = Table('user_data', metadata, autoload_with=engine)

        # Connect to the database and execute the query
        with engine.connect() as connection:
            # Select all rows from the `users` table
            stmt = select(users_table).where(users_table.c.dni == user_dni)
            result = connection.execute(stmt)

        users_list = [dict(row._mapping) for row in result]
        basic_data = users_list[-1]
    except:
        return redirect(url_for('main.logout'))
    if not basic_data:
        return redirect(url_for('main.logout'))

    session["cliente"] = basic_data["cliente"]
    session["dni"] = basic_data["dni"]
    return render_template("users/basic_data.html", data=basic_data, contact_phone=contact_phone.phone)


@main.route("/pagos_realizados", methods=["GET", "POST"])
@login_required
def payment_data():
    payment_data_scraper = Scraper()
    try:
        nro_producto = session['nro_producto']
    except KeyError:
        return redirect(url_for('main.user_data'))
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    payment_data = payment_data_scraper.get_payment_data(client_code, nro_producto, inicio_mora)
    if not payment_data:
        return redirect(url_for('main.logout'))
    session['total_debt'] = payment_data['total_debt']
    return render_template("users/pagos_realizados.html", data=payment_data)


@main.route('/detalle_pago/<payment_date>', methods=["GET", "POST"])
@login_required
def get_payment_detail(payment_date):
    payment_detail_data_scraper = Scraper()
    try:
        nro_producto = session['nro_producto']
        inicio_mora = session['inicio_mora']
        client_code = session['client_code']
    except KeyError:
        return redirect(url_for('main.user_data'))
    payment_detail_data = payment_detail_data_scraper.get_payment_detail(client_code, nro_producto, inicio_mora,
                                                                         payment_date)
    if not payment_detail_data:
        return redirect(url_for('main.logout'))
    return render_template("users/payment_details.html", data=payment_detail_data)


@main.route('/obtener_recibo/<payment_date>', methods=["GET", "POST"])
@login_required
def get_receipt(payment_date):
    receipt_scraper = Scraper()
    try:
        nro_producto = session['nro_producto']
        inicio_mora = session['inicio_mora']
        client_code = session['client_code']
    except KeyError:
        return redirect(url_for('main.user_data'))
    receipt_data = receipt_scraper.get_payment_receipt(client_code, nro_producto, inicio_mora, payment_date)
    if not receipt_data:
        return redirect(url_for('main.logout'))
    return render_template("users/payment_receipt.html", data=receipt_data)


@main.route("/nuevo_pago", methods=["GET", "POST"])
@login_required
def new_payment():
    new_payment_data_scraper = Scraper()
    try:
        if session['total_deuda'] == '0,00' or session['refinancing']:
            return redirect(url_for('main.user_data'))
    except KeyError:
        return redirect(url_for('main.user_data'))
    nro_producto = session['nro_producto']
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    risk = session['risk']
    assignment_date = session['assignment_date']
    capital = session['capital']
    int_punit = session['int_punit']
    int_finan = session['int_finan']
    new_payment_data = new_payment_data_scraper.get_new_payment(client_code, nro_producto, inicio_mora)
    if not new_payment_data:
        return redirect(url_for('main.logout'))

    payment_amount, max_installments_number, expiring_date, discount_exist = payment_conditions.apply_conditions \
        (new_payment_data, current_user.server_username, assignment_date, risk, capital, int_punit, int_finan)
    session['max_installments_number'] = max_installments_number
    session['discount_exist'] = discount_exist
    session['payment_amount'] = payment_amount
    session['total_debt'] = new_payment_data['total_debt']
    session['max_cuotas_quita'] = new_payment_data['max_cuotas_quita']
    session['txt_CNTx_CQ_field'] = new_payment_data['txt_CNTx_CQ_field']
    session['vto_primera_cuota'] = new_payment_data['vto_primera_cuota']
    session['vto_cada'] = new_payment_data['vto_cada']
    session['vto_dias'] = new_payment_data['vto_dias']
    session['address'] = new_payment_data['address']
    session['min_payment'] = new_payment_data['min_payment']
    op_phone = current_user.op_phone
    today = datetime.date.today()
    pa_number = float(payment_amount.replace(',', '.'))
    payment_amount_fmt = locale.format('%10.2f', pa_number, grouping=True)
    return render_template("users/new_payment.html", data=new_payment_data, op_phone=op_phone, today=today,
                           expiring_date=expiring_date, payment_amount=payment_amount_fmt,
                           max_installments_number=max_installments_number, discount_exist=discount_exist)


@main.route("/ver_refinanciacion/<payment_type>/<selected_installments_number>/<selected_exp_date>",
            methods=["GET", "POST"])
@login_required
def get_refinancing(payment_type, selected_installments_number, selected_exp_date):
    try:
        if session['total_deuda'] == '0,00' or session['refinancing']:
            return redirect(url_for('main.user_data'))
    except KeyError:
        return redirect(url_for('main.user_data'))
    payment_type = payment_type
    get_refinancing_data_scraper = Scraper()
    try:
        nro_producto = session['nro_producto']
    except KeyError:
        return redirect(url_for('main.logout'))
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    total_debt = session['total_debt']
    max_cuotas_quita = session['max_cuotas_quita']
    txt_CNTx_CQ_field = session['txt_CNTx_CQ_field']
    payment_amount = session['payment_amount']
    max_installments_number = session['max_installments_number']
    vto_primera_cuota = selected_exp_date.replace('-', '/')
    session['vto_primera_cuota'] = vto_primera_cuota
    vto_cada = session['vto_cada']
    vto_dias = session['vto_dias']
    address = session['address']
    selected_installments_number = int(selected_installments_number)
    if selected_installments_number > max_installments_number:
        selected_installments_number = max_installments_number

    selected_installments_number = str(selected_installments_number)
    session['selected_installments_number'] = selected_installments_number
    refinancing_data = get_refinancing_data_scraper.get_refinancing_info(client_code, nro_producto, inicio_mora,
                                                                         total_debt,
                                                                         max_cuotas_quita, txt_CNTx_CQ_field,
                                                                         current_user.username, vto_primera_cuota,
                                                                         vto_cada,
                                                                         vto_dias, address,
                                                                         selected_installments_number,
                                                                         payment_amount)
    if not refinancing_data:
        return redirect(url_for('main.logout'))
    op_phone = current_user.op_phone
    return render_template("users/refinancing_data.html", data=refinancing_data, payment_type=payment_type,
                           op_phone=op_phone)


@main.route("/refinanciaciones", methods=["GET", "POST"])
@login_required
def get_active_refinancing_data():
    get_active_refinancing_data_scraper = Scraper()
    nro_producto = session['nro_producto']
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']

    active_refinancing_data = get_active_refinancing_data_scraper.get_active_refinancing_data(client_code, nro_producto,
                                                                                              inicio_mora)
    if not active_refinancing_data:
        return redirect(url_for('main.logout'))

    op_phone = current_user.op_phone

    if current_user.got_refinancing:
        delete_refinancing = True
    else:
        delete_refinancing = False
    return render_template("users/refinanciaciones.html", data=active_refinancing_data, op_phone=op_phone,
                           delete_refinancing=delete_refinancing)


@main.route('/emision_refinanciacion', methods=["GET", "POST"])
@login_required
def get_refinancing_receipt():
    refinancing_receipt_scraper = Scraper()
    nro_producto = session['nro_producto']
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    refinancing_receipt_data = refinancing_receipt_scraper.get_refinancing_receipt(client_code, nro_producto,
                                                                                   inicio_mora)
    if not refinancing_receipt_data:
        return redirect(url_for('main.logout'))
    return render_template("users/refinancing_receipt.html", data=refinancing_receipt_data)


@main.route('/actualizar_nueva_refinanciacion/<payment_type>', methods=["GET", "POST"])
@login_required
def update_refinancing(payment_type):
    update_refinancing_scraper = Scraper()
    nro_producto = session['nro_producto']
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    max_cuotas_quita = session['max_cuotas_quita']
    txt_CNTx_CQ_field = session['txt_CNTx_CQ_field']
    vto_primera_cuota = session['vto_primera_cuota']
    vto_cada = session['vto_cada']
    vto_dias = session['vto_dias']
    address = session['address']
    selected_installments_number = session['selected_installments_number']
    payment_amount = session['payment_amount']
    vto_primera_cuota_obj = datetime.datetime.strptime(vto_primera_cuota, '%d/%m/%Y').date()
    vto_primera_cuota_fmted = str(vto_primera_cuota_obj).replace('-', '')
    update_refinancing_data = update_refinancing_scraper.update_new_refinancing(client_code, nro_producto, inicio_mora,
                                                                                vto_cada, vto_dias,
                                                                                vto_primera_cuota_fmted, address,
                                                                                max_cuotas_quita,
                                                                                txt_CNTx_CQ_field,
                                                                                selected_installments_number,
                                                                                payment_amount)
    if update_refinancing_data:
        user = db.session.query(User).filter_by(username=current_user.username).first()
        user.got_refinancing = True
        db.session.commit()
        session['refinancing'] = 'AL DÍA'
        visit_counter.increase_refinancing_counter()

        if payment_type == '1':
            return redirect(url_for('main.pago_facil_payment'))
        elif payment_type == '2':
            return redirect(url_for('main.tarjeta_naranja_payment'))
        elif payment_type == '3':
            return redirect(url_for('main.account_transfer_payment'))
        else:
            return redirect(url_for('main.logout'))
    else:
        return redirect(url_for('main.logout'))


@main.route("/pago_facil_pago", methods=["GET", "POST"])
@login_required
def pago_facil_payment():
    return render_template("users/pago_facil_payment.html")


@main.route('/pago_tarjeta_naranja', methods=["GET", "POST"])
@login_required
def tarjeta_naranja_payment():
    office = session['office']
    return render_template("users/tarjeta_naranja_payment.html", data=office)


@main.route('/pago_transferencia_bancaria', methods=["GET", "POST"])
@login_required
def account_transfer_payment():
    contact_number = current_user.op_phone
    return render_template("users/bank_account_transfer_payment.html", data=contact_number)


@main.route('/ver_refinanciacion_creada', methods=["GET", "POST"])
@login_required
def get_created_refinancing():
    created_refinancing_scraper = Scraper()
    nro_producto = session['nro_producto']
    inicio_mora = session['inicio_mora']
    client_code = session['client_code']
    created_refinancing_data = created_refinancing_scraper.get_refinancing_after_created(client_code, nro_producto,
                                                                                         inicio_mora)
    if not created_refinancing_data:
        return redirect(url_for('main.logout'))
    return render_template("users/refinancing_receipt.html", data=created_refinancing_data)


@main.route('/eliminar_refinanciacion', methods=["GET", "POST"])
@login_required
def delete_refinancing():
    if current_user.got_refinancing:
        user = db.session.query(User).filter_by(username=current_user.username).first()
        user.got_refinancing = False
        db.session.commit()
        delete_refinancing_scraper = Scraper()
        nro_producto = session['nro_producto']
        inicio_mora = session['inicio_mora']
        client_code = session['client_code']
        delete_refinancing_data = delete_refinancing_scraper.delete_refinancing(client_code, nro_producto,
                                                                                inicio_mora)
        if delete_refinancing_data:
            flash('Refinanciación anulada', 'message')
            return redirect(url_for('main.user_data'))
        else:
            return redirect(url_for('main.logout'))
    else:
        flash('Operación prohibida', 'error')
        return redirect(url_for('main.user_data'))


@main.route('/libre_deuda', methods=["GET", "POST"])
@login_required
def get_no_debt_certificate():
    try:
        name = session['cliente']
        dni = session['dni']
        today = datetime.now().date()
        months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
                  'Noviembre', 'Diciembre']
        month_in_letters = months[today.month - 1]
        html = render_template("users/libre_deuda.html", name=name, dni=dni, today=today,
                                month_in_letters=month_in_letters)
        return render_pdf(HTML(string=html), download_filename='libre_deuda.pdf')
    except:
        flash("Vista no permitida", 'error')
        return redirect(url_for('main.logout'))


@main.route("/reporting")
@login_required
def reporting():
    """Generates an excel report with basic usage stats"""
    book = dbdict.from_db_to_dict()
    return excel.make_response_from_book_dict(book, "xlsx", file_name="reporting")


@main.route("/excel_reporting")
@login_required
def excel_reporting():
    book = dbdict.from_excel_db_to_dict()
    return excel.make_response_from_book_dict(book, "xlsx", file_name="excel_reporting")


@main.route("/visitas", methods=["GET"])
@login_required
def visits():
    if current_user.is_admin:
        visits = db.session.query(VisitCounter).filter_by(id=1).first()
        if visits:
            total_visits = visits.counter
        else:
            total_visits = None

        refinancing_visits = db.session.query(RefinancingCounter).filter_by(id=1).first()
        if refinancing_visits:
            total_refinancing_visits = refinancing_visits.counter
        else:
            total_refinancing_visits = None
    else:
        return redirect(url_for('main.users'))
    return render_template("visits.html", visits=total_visits, refinancing_visits=total_refinancing_visits)


@main.route("/condiciones_descuento", methods=["GET", "POST"])
@login_required
def discount_conditions():
    if current_user.is_admin:
        all_conditions = db.session.query(Conditions).order_by(Conditions.id).all()
    else:
        return redirect(url_for('main.users'))
    return render_template("condiciones_descuento.html", conditions=all_conditions)


@main.route("/agregar_condiciones_descuento", methods=["GET", "POST"])
def add_discount_conditions():
    if current_user.is_admin:
        conditions = Conditions(code='a0600163', assigned_time_less_than=20, total_debt_less_than=25000,
                                total_debt_greater_than=10000, interests_discount=30, capital_discount=10,
                                installments_number=5, expiration_date=30, risk="NAR-BAJO")
        db.session.add(conditions)
        db.session.commit()
        return redirect(url_for('main.users'))


@main.route("/salvar_condiciones_descuento", methods=["POST"])
@login_required
def save_discount_conditions():
    req = request.get_json()
    for key, value in req.items():
        if value != '':
            if key != 'code' and key != 'risk' and key != 'max_discount':
                req[key] = int(value)
        else:
            if key == 'installments_number':
                req[key] = 1
            else:
                req[key] = None

    if req['max_discount'] is True:
        if req.get('interests_discount'):
            req['interests_discount'] = None
        if req.get('capital_discount'):
            req['capital_discount'] = None

    c = Conditions(code=req['code'], assigned_time_less_than=req['assigned_time_less_than'],
                   assigned_time_greater_than=req['assigned_time_greater_than'],
                   total_debt_less_than=req['total_debt_less_than'],
                   total_debt_greater_than=req['total_debt_greater_than'], risk=req['risk'],
                   interests_discount=req['interests_discount'], capital_discount=req['capital_discount'],
                   installments_number=req['installments_number'], expiration_date=req['expiration_date'],
                   max_discount=req['max_discount'])
    db.session.add(c)
    db.session.commit()
    res = make_response(jsonify(req), 200)
    return res


@main.route("/actualizar_condicion_descuento", methods=["POST"])
@login_required
def update_discount_conditions():
    if current_user.is_admin:
        req = request.get_json()
        for key, value in req.items():
            if value != '':
                if key != 'code' and key != 'risk' and key != 'max_discount':
                    req[key] = int(value)
            else:
                req[key] = None

        if req['max_discount'] is True:
            if req.get('interests_discount'):
                req['interests_discount'] = None
            if req.get('capital_discount'):
                req['capital_discount'] = None

        c = db.session.query(Conditions).filter_by(id=req['id']).one()
        if req.get('code'):
            c.code = req['code']
        else:
            c.code = None
        if req.get('assigned_time_less_than'):
            c.assigned_time_less_than = req['assigned_time_less_than']
        else:
            c.assigned_time_less_than = None
        if req.get('assigned_time_greater_than'):
            c.assigned_time_greater_than = req['assigned_time_greater_than']
        else:
            c.assigned_time_greater_than = None
        if req.get('total_debt_less_than'):
            c.total_debt_less_than = req['total_debt_less_than']
        else:
            c.total_debt_less_than = None
        if req.get('total_debt_greater_than'):
            c.total_debt_greater_than = req['total_debt_greater_than']
        else:
            c.total_debt_greater_than = None
        if req.get('risk'):
            c.risk = req['risk']
        else:
            c.risk = None
        if req.get('interests_discount'):
            c.interests_discount = req['interests_discount']
        else:
            c.interests_discount = None
        if req.get('capital_discount'):
            c.capital_discount = req['capital_discount']
        else:
            c.capital_discount = None
        if req.get('installments_number'):
            c.installments_number = req['installments_number']
        else:
            c.installments_number = 1
        if req.get('expiration_date'):
            c.expiration_date = req['expiration_date']
        else:
            c.expiration_date = None
        if req.get('max_discount'):
            c.max_discount = req['max_discount']
        else:
            c.max_discount = False

        db.session.commit()
    res = make_response(jsonify(req), 200)
    return res


@main.route("/eliminar_condicion_descuento/<condition_id>", methods=["GET", "POST"])
@login_required
def remove_discount_conditions(condition_id):
    if current_user.is_admin:
        condition = db.session.query(Conditions).filter_by(id=condition_id).one()
        db.session.delete(condition)
        db.session.commit()
        return redirect(url_for('main.discount_conditions'))
    else:
        flash("No tienes permisos para borrar esta condición.", "error")
        return redirect(url_for("main.discount_conditions")), 403


@main.route('/subir_imagen', methods=["GET", "POST"])
@login_required
def upload_image():
    form = ImageFileForm()
    if request.method == "POST":
        if current_user.is_admin:
            title = request.form['title']
            description = request.form['description']
            data = request.files['file'].read()
            image = FileContents(title=title, description=description, data=data)
            db.session.add(image)
            db.session.commit()
            flash("Imagen subida existosamente", 'message')
        else:
            return redirect(url_for('main.users'))
    return render_template("upload_image.html", form=form)


@main.route('/actualizar_imagen/<image_id>', methods=["POST"])
@login_required
def update_image(image_id):
    form = ImageFileForm()
    if request.method == "POST":
        if current_user.is_admin:
            title = request.form['title']
            description = request.form['description']
            file = request.files
            image = db.session.query(FileContents).filter_by(id=image_id).first()
            image.title = title
            image.description = description
            if file:
                data = request.files['file'].read()
                image.data = data
            db.session.commit()
        else:
            return redirect(url_for('main.users'))
    return render_template("upload_image.html", form=form)


@main.route('/ver_imagen/<image_id>')
@login_required
def show_image(image_id):
    image = db.session.query(FileContents).filter_by(id=image_id).one_or_none()
    return render_template("show_image.html", image=image)


@main.route('/imagenes')
@login_required
def show_images():
    all_images = db.session.query(FileContents).order_by(FileContents.id).all()
    return render_template("images.html", images=all_images)


@main.route('/servir_imagen/<id>')
@login_required
def serve_image(id):
    image = db.session.query(FileContents).filter_by(id=id).one_or_none()
    if image:
        image_binary = BytesIO(image.data)
        file_to_send = send_file(image_binary, as_attachment=False, attachment_filename="")
        return file_to_send, file_to_send.status_code
    else:
        return render_template("show_image.html", image=None)


@main.route("/eliminar_imagen/<image_id>", methods=["GET", "POST"])
@login_required
def remove_image(image_id):
    if current_user.is_admin:
        image = db.session.query(FileContents).filter_by(id=image_id).one()
        db.session.delete(image)
        db.session.commit()
        return redirect(url_for('main.show_images'))
    else:
        flash("No tienes permisos para borrar esta condición.", "error")
        return redirect(url_for("main.show_images")), 403


@main.route("/test")
@login_required
def reporting_test():
    """Generates an excel report with basic usage stats"""
    book = dbdict.get_excel_from_db()
    return send_file(book, mimetype='application/vnd.ms-excel', as_attachment=True,
                     attachment_filename="reporting.xlsx")

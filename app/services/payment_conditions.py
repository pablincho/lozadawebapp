from app.models import Conditions
from app import db
import datetime
from sqlalchemy import or_


def find_condition(server_username, assignment_date, total_debt, risk):
    today = datetime.date.today()

    total_debt = float(total_debt.replace(',', '.'))
    assignment_date_obj = datetime.datetime.strptime(assignment_date, '%d/%m/%Y').date()
    time_dif = today - assignment_date_obj
    days_dif = time_dif.days

    condition_query = db.session.query(Conditions).filter(or_(Conditions.code == server_username, Conditions.code == None),
                                                    or_(Conditions.assigned_time_less_than > days_dif,
                                                        Conditions.assigned_time_less_than == None),
                                                    or_(Conditions.assigned_time_greater_than < days_dif,
                                                        Conditions.assigned_time_greater_than == None),
                                                    or_(Conditions.total_debt_less_than > total_debt,
                                                        Conditions.total_debt_less_than == None),
                                                    or_(Conditions.total_debt_greater_than < total_debt,
                                                        Conditions.total_debt_greater_than == None),
                                                    or_(Conditions.risk == risk, Conditions.risk == None))
    try:
        condition = condition_query.one_or_none()
    except MultipleResultsFound:
        condition = condition_query.first()

    return condition


def apply_conditions(payment_data, server_username, assignment_date, risk, capital, int_punit, int_finan):
    total_debt = payment_data['total_debt']

    today = datetime.date.today()

    min_payment = payment_data['min_payment']
    min_payment = float(min_payment.replace(',', '.'))

    discount_exist = False
    condition = find_condition(server_username, assignment_date, total_debt, risk)

    if condition:
        capital = capital.replace('.', '')
        capital = float(capital.replace(',', '.'))

        int_punit = int_punit.replace('.', '')
        int_punit = float(int_punit.replace(',', '.'))

        int_finan = int_finan.replace('.', '')
        int_finan = float(int_finan.replace(',', '.'))

        interests = int_punit + int_finan

        if condition.max_discount:
            payment_amount = payment_data['min_payment']
            discount_exist = True
        else:
            if condition.capital_discount:
                capital_discount = 1 - condition.capital_discount / 100
                discount_exist = True
            else:
                capital_discount = 1
            if condition.interests_discount:
                interests_discount = 1 - condition.interests_discount / 100
                discount_exist = True
            else:
                interests_discount = 1

            if not discount_exist:
                payment_amount = payment_data['total_debt']
            else:
                sub_total = capital * capital_discount + interests * interests_discount
                fee = sub_total * 0.15
                tax_fee = fee * 0.21

                payment_amount = round(sub_total + fee + tax_fee, 2)  # Monto 18336,04
                if payment_amount < min_payment:
                    payment_amount = min_payment

                payment_amount = str(payment_amount)
                payment_amount = payment_amount.replace('.', ',')

        if condition.installments_number:
            installments_number = condition.installments_number
            if installments_number > int(payment_data['max_cuotas_quita']):
                installments_number = int(payment_data['max_cuotas_quita'])
        else:
            installments_number = 1

        if condition.expiration_date:
            expiring_date = str(today + datetime.timedelta(days=condition.expiration_date))  # today, object 2020-01-10
        else:
            expiring_date_obj = datetime.datetime.strptime(payment_data['vto_primera_cuota'], '%d/%m/%Y').date()
            expiring_date = str(expiring_date_obj)
    else:
        payment_amount = payment_data['total_debt']
        installments_number = 1
        expiring_date = str(datetime.datetime.strptime(payment_data['vto_primera_cuota'], '%d/%m/%Y').date())

    return payment_amount, installments_number, expiring_date, discount_exist

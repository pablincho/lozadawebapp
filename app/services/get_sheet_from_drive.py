import os
import pandas as pd
from flask import jsonify
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from sqlalchemy import create_engine
from werkzeug.security import generate_password_hash
from sqlalchemy import create_engine
import logging

# Configure logging to output to console for demonstration
logging.basicConfig(level=logging.ERROR)
# Path to your service account key file
SERVICE_ACCOUNT_FILE = 'alert-snowfall-321418-9429ec739071.json'

# Define the required scopes
SCOPES = ['https://www.googleapis.com/auth/drive.readonly']

# Google Drive file ID for the Excel file you want to access
SPREADSHEET_ID = '13T4Ecg9BdHRUSlXtNPUGX-cvaIvaLNRZ2zqBZecYuNQ'

EXCEL_COLUMNS = ['DNI', 'Cliente', 'Ultimo Pago', 'Alta Plan', 'Cuota No Pagada', 'Est del PP',
                 'Cuotas Pagadas de pp activo', 'Saldo cuota que todavia no pagó',
                 'Cuotas Originales PP Activo', 'Cuota Original', 'Saldo Plan']

COLUMNS_MAPPER = {
    'DNI': 'dni',
    'Cliente': 'cliente',
    'Nombre': 'cliente',
    'Est del PP': 'estado_del_pp',
    'Ultimo Pago': 'ultimo_pago',
    'Alta Plan': 'alta_plan',
    'Cuota No Pagada': 'cuota_no_pagada',
    'Cuotas Pagadas de pp activo': 'cuotas_pagadas_pp_activo',
    'Saldo cuota que todavia no pagó': 'saldo_cuota_aun_no_pago',
    'Cuotas Originales PP Activo': 'cuotas_orig_pp_activo',
    'Cuota Original': 'cuota_original',
    'Saldo Plan': 'saldo_plan'
}
def get_sheets_service():
    # Authenticate using the service account
    service_account_info = {
        "type": os.getenv("GOOGLE_TYPE"),
        "project_id": os.getenv("GOOGLE_PROJECT_ID"),
        "private_key_id": os.getenv("GOOGLE_PRIVATE_KEY_ID"),
        "private_key": os.getenv("GOOGLE_PRIVATE_KEY").replace("\\n", "\n"),  # Ensure correct newline formatting
        "client_email": os.getenv("GOOGLE_CLIENT_EMAIL"),
        "client_id": os.getenv("GOOGLE_CLIENT_ID"),
        "auth_uri": os.getenv("GOOGLE_AUTH_URI"),
        "token_uri": os.getenv("GOOGLE_TOKEN_URI"),
        "auth_provider_x509_cert_url": os.getenv("GOOGLE_AUTH_PROVIDER_X509_CERT_URL"),
        "client_x509_cert_url": os.getenv("GOOGLE_CLIENT_X509_CERT_URL"),
    }
    # Create credentials object
    credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=SCOPES)

    # Build the Google Drive API service
    # service = build('drive', 'v3', credentials=credentials)
    sheets_service = build('sheets', 'v4', credentials=credentials)
    return sheets_service


def get_sheet_and_save_to_db(app):
    conn_string = os.getenv('DATABASE_URL')
    if conn_string and conn_string.startswith("postgres://"):
        conn_string = conn_string.replace("postgres://", "postgresql://", 1)
    engine = create_engine(conn_string)

    try:
        # Get the Google Drive service
        sheets_service = get_sheets_service()
        # Get metadata to identify the first sheet's name
        spreadsheet_metadata = sheets_service.spreadsheets().get(spreadsheetId=SPREADSHEET_ID).execute()
        sheets = spreadsheet_metadata.get('sheets', [])
        first_sheet_name = sheets[0].get("properties", {}).get("title")
        # Define the ranges to fetch
        first_sheet_range = f"{first_sheet_name}!A:S"  # Get all columns A to S in the first sheet
        cancelados_sheet_range = 'Cancelados!A:B'      # Get all columns A to B in 'Cancelados'

        # Use batchGet to fetch both ranges in a single API call
        result = sheets_service.spreadsheets().values().batchGet(
            spreadsheetId=SPREADSHEET_ID,
            ranges=[first_sheet_range, cancelados_sheet_range]
        ).execute()
        # Extract data from the API response
        first_sheet_data_raw = result['valueRanges'][0].get('values', [])
        cancelados_sheet_data_raw = result['valueRanges'][1].get('values', [])
        # Convert to pandas DataFrames
        # Use the first row as the header if it exists, otherwise fallback to default integer headers
        sheet_gestion = pd.DataFrame(first_sheet_data_raw[1:], columns=first_sheet_data_raw[0]) if first_sheet_data_raw else pd.DataFrame()
        sheet_cancelados = pd.DataFrame(cancelados_sheet_data_raw[1:], columns=cancelados_sheet_data_raw[0]) if cancelados_sheet_data_raw else pd.DataFrame()

        sheet_gestion = sheet_gestion[EXCEL_COLUMNS]
        sheet_gestion = sheet_gestion.rename(columns=COLUMNS_MAPPER)
        sheet_cancelados = sheet_cancelados.rename(columns=COLUMNS_MAPPER)
        sheet_cancelados['estado_del_pp'] = 'Deuda cancelada'
        debt_info = pd.merge(sheet_gestion, sheet_cancelados, on=['dni', 'cliente', 'estado_del_pp'], how='outer')
        debt_info.to_sql('user_data', engine, if_exists='replace', index=True)

        # if username in db is no longer in user_data remove
        new_users_list = debt_info['dni'].astype(str).to_list()
        from app import db
        from app.models import User
        with app.app_context():
            existing_users_list = db.session.query(User.username).filter_by(role_id=1).all()
            existing_users_list = [elem[0] for elem in existing_users_list]

            users_to_delete = [user for user in existing_users_list if user not in new_users_list]
            if users_to_delete:
                db.session.query(User).filter(User.username.in_(users_to_delete)).delete(synchronize_session=False)
                db.session.commit()
            # insert new users if they are not in the db
            common_password = os.getenv("DEFAULT_USER_PASSWORD")
            password_hash = generate_password_hash(common_password)
            existing_users_set = set(existing_users_list)
            users_to_insert = [
                {"username": new_user, "password_hash": password_hash, "role_id": 1}
                for new_user in new_users_list if new_user not in existing_users_set
            ]
            if users_to_insert:
                db.session.bulk_insert_mappings(User, users_to_insert)
                db.session.commit()
        print("Sheet fetched")
    except Exception as e:
        logging.exception(e)
        return jsonify({"error": str(e)})



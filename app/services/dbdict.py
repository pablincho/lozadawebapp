import pytz
from app.models import User

def from_db_to_dict() -> dict:
    """Extracts the usage data from the database"""
    db_data_dict = {}
    users_value = [["DNI", "Teléfono", "Ultimo login"]]
    users = User.query.filter_by(role_id=1).all()
    for element in users:
        if element.last_login:
            last_login = element.last_login.astimezone(pytz.timezone("America/Argentina/Cordoba")).strftime("%d-%m-%Y %H:%M")
        else:
            last_login = ''
        if element.phone:
            phone = element.phone
        else:
            phone = ''
        users_value.append([element.username, phone, last_login])

    db_data_dict.update({"Usuarios": users_value})
    return db_data_dict

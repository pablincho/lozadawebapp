from app import db
from app.models import VisitCounter, RefinancingCounter
from sqlalchemy.orm.exc import NoResultFound


def increase_visit_counter():
    try:
        visit_counter = db.session.query(VisitCounter).filter_by(id=1).first()
        if visit_counter:
            visit_counter.counter += 1
            db.session.commit()
        else:
            visit_counter = VisitCounter(id=1, counter=1)
            db.session.add(visit_counter)
            db.session.commit()
    except NoResultFound:
        visit_counter = VisitCounter(id=1, counter=1)
        db.session.add(visit_counter)
        db.session.commit()


def increase_refinancing_counter():
    try:
        refinancing_counter = db.session.query(RefinancingCounter).filter_by(id=1).first()
        if refinancing_counter:
            refinancing_counter.counter += 1
            db.session.commit()
        else:
            refinancing_counter = RefinancingCounter(id=1, counter=1)
            db.session.add(refinancing_counter)
            db.session.commit()
    except NoResultFound:
        refinancing_counter = RefinancingCounter(id=1, counter=1)
        db.session.add(refinancing_counter)
        db.session.commit()

from app import db
from flask import request, flash
from app.models import User
from io import BytesIO
import pandas as pd

def insert_users():
    users_ctr = 0
    users_mdf = 0
    file = request.files["sheet_file"]
    file_stream = BytesIO(file.read())
    first_sheet = pd.read_excel(file_stream, sheet_name=0)
    rows = first_sheet.itertuples(index=False, name=None)
    for rowIndex, row in enumerate(rows):
        if rowIndex <= 140:
            try:
                user_cell = str(row[0])
                password_cell = str(row[1])
                op_name_cell = str(row[2])
                server_user_cell = str(row[3])
                op_phone_cell = str(row[4])
                pp_status_cell = str(row[5])
                if row[6] == '':
                    role_cell = 1
                else:
                    role_cell = int(row[6])
                assignment_date_cell = str(row[7])
                if pd.isna(row[8]):
                    phone = ''
                else:
                    phone = str(row[8])
            except IndexError:
                flash("Archivo excel en formato incorrecto", 'error')
                return
            existing_user = db.session.query(User).filter_by(username=user_cell).one_or_none()
            if existing_user:
                existing_user.password = password_cell
                existing_user.op_phone = op_phone_cell
                existing_user.server_username = server_user_cell
                existing_user.op_name = op_name_cell
                existing_user.pp_status = pp_status_cell
                existing_user.role_id = role_cell
                existing_user.assignment_date = assignment_date_cell
                existing_user.phone = phone
                db.session.commit()
                users_mdf += 1
            else:
                u = User(
                    username=user_cell,
                    password=password_cell,
                    op_phone=op_phone_cell,
                    role_id=role_cell,
                    server_username=server_user_cell,
                    op_name=op_name_cell,
                    pp_status=pp_status_cell,
                    assignment_date=assignment_date_cell,
                    phone=phone
                )
                db.session.add(u)
                users_ctr += 1
    db.session.commit()
    if users_ctr:
        flash("{} usuarios agregados".format(users_ctr), "message")
    if users_mdf:
        flash("{} usuarios modificados".format(users_mdf), "message")
    return

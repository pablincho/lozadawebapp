from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from .permissions import Permission


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), index=True, unique=True) # DNI
    password_hash = db.Column(db.String(512))
    phone = db.Column(db.String(50))
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))
    last_login = db.Column(db.DateTime(timezone=True))
    op_phone = db.Column(db.String(50))
    server_username = db.Column(db.String(100))
    op_name = db.Column(db.String(100))
    pp_status = db.Column(db.String(100))
    assignment_date = db.Column(db.String(50))

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @property
    def is_admin(self) -> bool:
        """Checks if user is admin"""
        if self.role_id == 3:
            return True
        else:
            return False

    @property
    def is_super(self) -> bool:
        """Checks if user is super"""
        if self.role_id == 2:
            return True
        else:
            return False

    @property
    def is_normal(self) -> bool:
        if self.role_id == 1:
            return True
        else:
            return False

    @staticmethod
    def insert_admin_user(username, password):
        user = db.session.query(User).filter_by(username=username).one_or_none()
        if user is None:
            u = User(username=username, password=password, role_id=3)
            db.session.add(u)
            db.session.commit()

    @staticmethod
    def insert_test_user(username, password):
        """Inserts an test user"""
        user = (
            db.session.query(User).filter_by(username=username).one_or_none()
        )
        if user is None:
            u = User(
                username=username,
                password=password,
                role_id=1,
                phone=3515678901,
                op_phone=3516789012
            )
            db.session.add(u)
            db.session.commit()

    def __repr__(self):
        return "<User> Username -  '{}'".format(self.username)


class Role(db.Model, UserMixin):
    """Model which contains the role of an app user"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship("User", backref="role", lazy="dynamic")

    @staticmethod
    def insert_roles():
        """Fills in roles table with current valid roles"""
        roles = {
            "Usuario": (Permission.USER, True),
            "Super": (Permission.SUPER, False),
            "Administrador": (0xFF, False),
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return "<Role %r>" % self.name


class ContactPhone(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(64))

    def __repr__(self):
        return "<Contact Phone %r>" % self.phone


class VisitCounter(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    counter = db.Column(db.Integer)

    def __repr__(self):
        return "<Visit Counter %r>" % self.counter


class RefinancingCounter(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    counter = db.Column(db.Integer)

    def __repr__(self):
        return "<Refinancing Counter %r>" % self.counter


class Conditions(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(100))
    assigned_time_less_than = db.Column(db.Integer)
    assigned_time_greater_than = db.Column(db.Integer)
    total_debt_less_than = db.Column(db.Integer)
    total_debt_greater_than = db.Column(db.Integer)
    risk = db.Column(db.String(100))
    interests_discount = db.Column(db.Integer)
    capital_discount = db.Column(db.Integer)
    installments_number = db.Column(db.Integer)
    expiration_date = db.Column(db.Integer)
    max_discount = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return "<Conditions> '{}'".format(self.code)


class FileContents(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(400))
    data = db.Column(db.LargeBinary)

    def __repr__(self):
        return "<FileContents> '{}'".format(self.name)


class UserData(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    dni = db.Column(db.Text())
    cliente = db.Column(db.Text())
    ultimo_pago = db.Column(db.Text())
    alta_plan = db.Column(db.Text())
    cuota_no_pagada = db.Column(db.Text())
    estado_del_pp = db.Column(db.Text())
    cuotas_pagadas_pp_activo = db.Column(db.Text())
    saldo_cuota_aun_no_pago = db.Column(db.Text())
    cuotas_orig_pp_activo = db.Column(db.Text())
    cuota_original = db.Column(db.Text())
    saldo_plan = db.Column(db.Text())

    def __repr__(self):
        return "<UserData> '{}'".format(self.operador)

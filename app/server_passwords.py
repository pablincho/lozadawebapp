import os


def get_server_password(user):
    return os.environ.get(user)


if __name__ == '__main__':
    user = 'a0600019'
    password = get_server_password(user)
    print(password)
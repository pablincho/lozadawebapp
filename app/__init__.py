import os
from flask import Flask, render_template
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_wtf.csrf import CSRFProtect
from app.services.get_sheet_from_drive import get_sheet_and_save_to_db
import flask_excel
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import timezone
import atexit

db = SQLAlchemy()

SCHEDULER_HOUR = os.getenv("SCHEDULER_HOUR") or "10"
SCHEDULER_MINUTE = os.getenv("SCHEDULER_MINUTE") or "00"

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    # tasks scheduler
    scheduler = BackgroundScheduler()
    # Configure the task to run every day at 10:00 AM Eastern Time (ET)
    scheduler.add_job(func=get_sheet_and_save_to_db, trigger="cron", hour=int(SCHEDULER_HOUR),
                      minute=int(SCHEDULER_MINUTE), timezone=timezone("America/Cordoba"),
                      kwargs={"app": app})
    scheduler.start()
    # Shut down the scheduler when exiting the app
    atexit.register(lambda: scheduler.shutdown())
    login_manager = LoginManager()
    login_manager.login_view = "main.login"
    login_manager.init_app(app)
    CSRFProtect(app)
    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from app.commands.users import cmd as users_cmd
    app.cli.add_command(users_cmd)
    flask_excel.init_excel(app)
    db.init_app(app)
    Migrate(app, db)

    @app.errorhandler(500)
    def internal_error(error):
        """Renders template for 500 error"""
        return render_template('500.html'), 500

    return app
